<?php require_once 'inc/sessions.php'; ?>
<?php require_once 'inc/db_connect.php'; ?>
<?php confirm_login(); ?>
<?php
if (isset($_POST['submit'])) {
   $error=array();
  $rq=array("email","password");
  foreach ($rq as $value) {
    if (empty($_POST[$value]) || !isset($_POST[$value])) {
      $error[]= $value;
    }
  }

  if (empty($error)) {
    
    $email=mysqli_real_escape_string($con,$_POST['email']);
    $password=mysqli_real_escape_string($con,$_POST['password']);
    $pass=hash('ripemd128', $password);
    //check if user eist
    $ck=mysqli_query($con,"select * from users where email ='$email' and password='$pass' LIMIT 1");
    if (mysqli_num_rows($ck)>0) {
      #user exist
      $found_user=mysqli_fetch_array($ck);
      $_SESSION['email']=$found_user['email'];
      $_SESSION['name']=$found_user['fullname'];
      header("Location:starter.php");

    }
    else{
      #user does not exist
       $msg_error="Email and Password do not match ";
    }
}
else{
  $msg_error="There are ".count($error)." in you form";
}
}

?>

<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Tetrashare | Set UP</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="dist/css/skins/skin-yellow-light.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#"><b>Tetra</b>Share</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">
  <form >
    <select>
  <option>General Fora</option>
</select>
<select>
  <option>Inspirational forum</option>
</select>
<button class="btn btn-primary">Go!</button>
  </form>


  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->
<script src="../../bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="../../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="../../plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
