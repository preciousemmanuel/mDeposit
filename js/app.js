$(document).ready(function(){




	//function that clears post message
  setTimeout(function(){
         $("#nd").hide(500);
        },5000);

})

// register system
//overlay
var $overlay=$('<div id="overlay"></div>');
var $successMsg=$('<h3 id="successMsg"></h3>');
$('body').append($overlay);
$(document).ready(function(){
	$(document).on('submit','#register',function(e){
		e.preventDefault();
		$overlay.append('<img src="img/6.gif" id="load-img-reg">');
		$overlay.show();
		var form=$(this);
		var data=form.serialize();
		$.ajax({
			type:'POST',
			url:"inc/register.php",
			data:data,
			cache:false,
			success:function(res){
				if(res==='success')
				{
					$successMsg.html("Account successfully created...")
					$overlay.append($successMsg);
					setTimeout(gotoDashboard(),20000);
				}
				else{

					$('#msg_error').text(res);
				$('#nd').show();
				$('#load-img-reg').remove();
				$overlay.hide();
				}
				
			}
		})
	})
})

//login system
$(document).ready(function(){
	$(document).on('submit','#login',function(e){
		e.preventDefault();
		$overlay.append('<img src="img/6.gif" id="load-img-reg">');
		$overlay.show();
		var form=$(this);
		var data=form.serialize();
		$.ajax({
			type:'POST',
			url:"inc/login.php",
			data:data,
			cache:false,
			success:function(res){
				if(res==='success')
				{
					$successMsg.html("Login successfull...")
					$overlay.append($successMsg);
					setTimeout(gotoDashboard(),20000);
				}
				else{

					$('#msg_errors').text(res);
				$('#nd').show();
				$('#load-img-reg').remove();
				$overlay.hide();
				}
				
			}
		})
	})
})
//call this function after success message in registration
function gotoDashboard(){
	window.location.href="dashboard.php"
}


function header(){
	$.ajax({
		method:'POST',
		url:"inc/nav.php",
		success:function(data){
			$('#headers').html(data);
		}
	})
}
$(document).ready(function(){
	header();
})
function show_dash(){
	$(document).on('click','#dash',function(e){

		e.preventDefault();
		
		dash();
	})
	// $.ajax({
	// 	method:'POST',
	// 	url:"inc/genral_subfora.php",
	// 	success:function(data){
	// 		$('#gen-sub').html(data);
	// 	}
	// })
}
$(document).ready(function(){
	show_dash();
})

//sucess transfer
// function succestran(){
	
// 	$.ajax({
// 		method:'POST',
// 		url:"inc/success_transc.php",
// 		success:function(data){
			
// 			$('#activity').html(data);
// 		}
// 	})
// }
// $(document).ready(function(){
// 	succestran();
// })
function sidebar(){
	$.ajax({
		method:'POST',
		url:"inc/sidebar.php",
		success:function(data){
			$('#sidebar').html(data);
		}
	})
}
$(document).ready(function(){
	sidebar();
})
function adminsidebar(){
	$.ajax({
		method:'POST',
		url:"inc/admin_side.php",
		success:function(data){
			$('#sidebars').html(data);
		}
	})
}
$(document).ready(function(){
	adminsidebar();
})
function dash(){
	$.ajax({
		method:'POST',
		url:"inc/dashboard.php",
		success:function(data){
			$('#main-data').html(data);
		}
	})
}
$(document).ready(function(){
	dash();
})
//admin dash
function gen_dash(){
	$.ajax({
		method:'POST',
		url:"inc/gen_dash.php",
		success:function(data){
			$('#gen_dash').html(data);
		}
	})
}
$(document).ready(function(){
	gen_dash();
})
function current_bal(){
	$.ajax({
		method:'POST',
		url:"inc/current_balance.php",
		success:function(data){
			$('#curr').html(data);
		}
	})
}
$(document).ready(function(){
	current_bal();
})

//num transaction
function num_tran(){
	$.ajax({
		method:'POST',
		url:"inc/num_tran.php",
		success:function(data){
			$('#tranx').html(data);
		}
	})
}
$(document).ready(function(){
	num_tran();
})

function bankdeposit(){
	$(document).on('click','#bankdeposit',function(e){

		e.preventDefault();
		$(this).addClass("currents")
		$('#load-img').show()
		$.ajax({
		method:'POST',
		url:"inc/bank_transfer.php",
		success:function(data){
			$('#show-datao').html(data);
			$('#load-img').hide()
		}
	})
	})
	
}
$(document).ready(function(){
	bankdeposit();
})
function wallet_trans(){
	$(document).on('click','#wallet_trans',function(e){

		e.preventDefault();
		$(this).addClass("currents")
		$('#load-img').show()
		$.ajax({
		method:'POST',
		url:"inc/wallet_transfer.php",
		success:function(data){
			$('#show-datao').html(data);
			$('#load-img').hide()
		}
	})
	})
	
}
$(document).ready(function(){
	wallet_trans();
})
//admin add pin
function addpin(){
	$(document).on('click','#addpin',function(e){

		e.preventDefault();
		var amount=$('#amount').val();
		$('#addpin').html('<img src="img/small.gif">');
		$.ajax({
		method:'POST',
		data:'amount='+amount,
		url:"inc/generate.php",
		success:function(data){
			if (data.indexOf("incorrect") > -1) {
				alert('pin is invalid')
			$('#addpin').html('Ok')
			} else if(data.indexOf("false") > -1){
				alert('failed to add pin');
			$('#addpin').html('Ok')
			}
			else if(data.indexOf("true") > -1){
				alert('You have successfully added pin');
				$('#amount').val('');
				gen_dash();
			$('#addpin').html('Ok')
			}
			else{
				alert(data)
			}
		}
	})
	})
	
}
$(document).ready(function(){
	addpin();
})
//recharge pin
function recharge(){
	$(document).on('click','#recharge',function(e){

		e.preventDefault();
		var pin=$('#pin').val();
		$('#recharge').html('<img src="img/small.gif">');
		$('#recharge').prop('disabled',true)
		$.ajax({
		method:'POST',
		data:'pin='+pin,
		url:"inc/recharge_pin.php",
		success:function(data){
			alert(data);
			$('#pin').val('');
			$('#recharge').prop('disabled',false)
				$('#recharge').html('Ok');
				current_bal();
				location.reload();
		}
	})
	})
	
}
$(document).ready(function(){
	recharge();
})
//change phone number
function chang_num(){
	$(document).on('click','#chng_phne',function(e){

		e.preventDefault();
		var phone=$('#phne').val();
		$('#chng_phne').html('<img src="img/small.gif">');
		$('#chng_phne').prop('disabled',true)
		$.ajax({
		method:'POST',
		data:'phone='+phone,
		url:"inc/change_number.php",
		success:function(data){
			if (data.indexOf("false") > -1) {
				alert(data);
			
			$('#chng_phne').prop('disabled',false)
				$('#chng_phne').html('Ok');
			}
			else{
				alert("you have successfully change phone number");
			$('#phne').val('');
			$('#chng_phne').prop('disabled',false)
				$('#chng_phne').html('Ok');
				$('#c_phone').html(data);
			}
			
				
				
		}
	})
	})
	
}
$(document).ready(function(){
	chang_num();
})
//make wallet transfer
function make_wallet_transfer(){
	$(document).on('submit','#wallet_transfer',function(e){

		e.preventDefault();
		var beneficiary_name=$('#reciever_name').val();
		var amount=$('#amount').val();
		if (confirm("You are about to transfer N"+amount+" to "+beneficiary_name+". A service charge of N50 will be deducted")) {
		var form=$(this);
		var data=form.serialize();
		$('#save').html('<img src="img/small.gif">');
		$('#save').prop('disabled',true)
		$.ajax({
		method:'POST',
		data:data,
		url:"inc/make_wallet_transfer.php",
		success:function(data){
			if (data.indexOf("success") > -1) {
			alert("transaction successful")
			$('#save').prop('disabled',false)
				$('#save').html('Ok');
				current_bal();
				num_tran();
				$('#wallet_transfer')[0].reset();
			}
			else if(data.indexOf("no fund") > -1){
				alert("Fund not enough,recharge your wallet and try again")
				$('#save').prop('disabled',false)
				$('#save').html('Ok');
				current_bal();
				num_tran()
			}
			else if(data.indexOf("wrong pin") > -1){
				alert("Your mDeposit pin is incorrect")
				$('#save').prop('disabled',false)
				$('#save').html('Ok');
				current_bal();
				num_tran()
			}
			else if(data.indexOf("false") > -1){
				alert("Beneficiary wallet id or phone number is incorrect")
				$('#save').prop('disabled',false)
				$('#save').html('Ok');
				current_bal();
				num_tran()
			}
			else{
				alert(data)
				$('#save').prop('disabled',false)
				$('#save').html('Ok');
				current_bal();
				num_tran()
			}
		}
	})
	}
	})
	
}
$(document).ready(function(){
	make_wallet_transfer();
})
function buyairtime(){
	$(document).on('click','#buyairtimes',function(e){

		e.preventDefault();
		$('#load-img').show()
		$.ajax({
		method:'POST',
		url:"inc/buy-airtime.php",
		success:function(data){
			$('#show-datao').html(data);
			$('#load-img').hide()
		}
	})
	})
	
}
$(document).ready(function(){
	buyairtime();
})
//change pin
function changepin(){
	$(document).on('click','#ok',function(e){

		e.preventDefault();
		var oldpin=$('#oldpin').val();
		var newpin=$('#new-pin').val();
		var confirmpin=$('#confirm-pin').val();
		if (oldpin!=""&& newpin!=""&& confirmpin!="") {
			$('#ok').html('<img src="img/small.gif">');
				$.ajax({
		method:'POST',
		url:"inc/changepin.php",
		data:{oldpin:oldpin,newpin:newpin,confirmpin:confirmpin},
		success:function(data){
			//$('#show-datao').html(data);
			if (data.indexOf("matchless") > -1) {
				alert('new password and confirm password do not match')
			$('#ok').html('Ok')
			} else if(data.indexOf("incorrect") > -1){
				alert('old password is incorrect');
			$('#ok').html('Ok')
			}
			else if(data.indexOf("true") > -1){
				alert('You have successfully changed pin');
				$('#oldpin').val('');
				$('#new-pin').val('');
				$('#confirm-pin').val('');
			$('#ok').html('Ok')
			}
			else{
				alert(data)
			}
			
		}
	})
		}
		

	
	})
	
}
$(document).ready(function(){
	changepin();
})
//check old pin
function oldpin(){
	$(document).on('blur','#oldpin',function(e){
		var pin=$(this).val();
		if (pin!="") {
			$('#oldpin-img').show();
			$.ajax({
			method:'POST',
				url:"inc/validatepin.php?cat=old",
				data:"pin="+pin,
			success:function(data){
			pins(data)
			
		}
	})
		}else{
			$('#oldpin-img').hide();
		}
		
		
	})
}

$(document).ready(function(){
	oldpin();
})
//walet reciever details
function reciever_details(){
	$(document).on('blur','#reciever_id',function(){
		
		var pin=$(this).val();
		if (pin!="") {
			//$('#reciever_name').val('<img src="img/small.png">');
			$.ajax({
			method:'POST',
				url:"inc/reciever_details.php",
				data:"pin="+pin,
			success:function(data){
				if (data.indexOf('false')>-1) {
					alert('wallet id or phone number does not match any user')
					$('#reciever_name').val(' ');
				}
				else{
			$('#reciever_name').val(data);
			}
		}
	})
		}else{
			$('#reciever_name').val(' ');
		}
		
		
	})
}

$(document).ready(function(){
	reciever_details();
})
function pins(data){
	var d=data;
	
if (d.indexOf("true") > -1) {
				$('#oldpin-img').attr('src','img/gud.png');
				
			} else{
				$('#oldpin-img').attr('src','img/cancel.png');
			}
}
function confirm_pin(){
	$(document).on('blur','#confirm-pin',function(){
		var confirmpin=$(this).val();
		var pin=$('#new-pin').val();
		if (confirmpin!="") {
		if (pin!=confirmpin ) {
			$('#conf-img').attr('src','img/cancel.png');
			$('#conf-img').show()
		
		}else{
			$('#conf-img').show()
			$('#conf-img').attr('src','img/gud.png');
		}
		}
		else{
			$('#conf-img').hide()
		}
		
	})
}
$(document).ready(function(){
	confirm_pin();
})
//post data for form processing
// $(document).ready(function(){
// 	$(document).on('submit','#edit-profile-form',function(e){
// 		e.preventDefault();
// 		var formdata=new FormData(this);
	
// 		$.ajax({
// 		method:'POST',
// 		url:"inc/editprofileform.php",
// 		data:formdata,
// 		enctype: 'multipart/form-data',
// 		cache:false,
//             contentType: false,
//             processData: false,
// 		success:function(data){
// 			alert(data);
// 			header();
// 			sidebar();
// 			editprofile();
// 		}
// 	})
// 	})
// })