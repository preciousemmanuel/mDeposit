<?php require_once 'sessions.php'; ?>
<?php require_once 'db_connect.php'; ?>
<?php confirm_login(); ?>
<?php require_once 'function.php'; ?>
<?php $user=get_each_user($_SESSION['id']); ?>

<div class="box" style="padding: 10px">
<h4>Wallet Transfer</h4>
	<form id="wallet_transfer">
		
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group has-feedback">
        <input type="text" required id="reciever_id" name="reciever_id" class="form-control" placeholder="Phone number or wallet ID of beneficiary">
        <span class="fa fa-user text-gray form-control-feedback"></span>
      </div>
			</div>
			<div class="col-lg-6">
				<div class="form-group has-feedback">
        <input type="number" required id="amount" name="amount" class="form-control" placeholder="Amount">
        <span class="fa fa-money text-gray form-control-feedback"></span>
      </div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group has-feedback">
        <input type="text" required id="reciever_name" name="reciever_name" class="form-control" placeholder="Name of beneficiary">
        <span class="fa fa-pencil text-gray form-control-feedback"></span>
      </div>
			</div>
			<div class="col-lg-6">
				<div class="form-group has-feedback">
        <input type="password" required class="form-control" name="pin" placeholder="Your mDeposit pin">
        <span class="fa fa-ticket text-gray form-control-feedback"></span>
      </div>
			</div>
		</div>
		<div class="row">
				<div class="col-md-6" style="margin-bottom: 1%">
			<button type="submit" id="save" class="btn btn-block btn-success">Proceed</button>
		</div>
		</div>
	</form>

</div>