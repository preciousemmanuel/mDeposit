<?php session_start(); ?>
<?php require_once 'db_connect.php'; ?>
<?php require_once 'random/lib/random.php'; ?>
<?php require_once 'function.php'; ?>
<?php
if (isset($_POST)) {
  $error=array();
  $rq=array("fullname","password","confirm","phone");
  foreach ($rq as $value) {
    if (empty($_POST[$value]) || !isset($_POST[$value])) {
      $error[]= $value;
    }
  }

  if (empty($error)) {
    $fullname=mysqli_real_escape_string($con,$_POST['fullname']);
    $email=mysqli_real_escape_string($con,$_POST['email']);
    $password=mysqli_real_escape_string($con,$_POST['password']);
    $confirm=mysqli_real_escape_string($con,$_POST['confirm']);
     $phone=mysqli_real_escape_string($con,$_POST['phone']);
    //check if password is the same
    if ($password!=$confirm) {
      echo "Password do not match ";
    }
   //check if user exit
    else{
    $ch=mysqli_query($con,"select * from users where phone='$phone' LIMIT 1");
    if (mysqli_num_rows($ch)>0) {
      //user exist
      echo "Account already exist";
    }
    else{
      //user does not exist
      //hash the password
      $hash=hash('ripemd128', $password);
      $walletid=randomString(4);
      $m=mysqli_query($con,"insert into users(fullname,email,pin,phone,wallet_id) values('$fullname','$email','$hash','$phone','$walletid')");
      if ($m) {
        # success...
         $_SESSION['id']=mysqli_insert_id($con);
        $_SESSION['email']=$email;
        $_SESSION['name']=$fullname;
        echo "success";
      }
      else{
        #failed
        echo "Failed to create account ".mysqli_error($con);
      }
    }
  }
}
  else{
    echo "There are ".count($error)." in you form";
  }
}

?>