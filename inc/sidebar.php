
<?php require_once 'db_connect.php'; ?>

<?php require_once 'function.php'; ?>
<?php $user=get_each_user($_SESSION['id']); ?>

<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        
        <div class="pull-left" style="color: white">
          <p><?php echo $user['fullname']; ?></p>
          <!-- Status -->
          <a href="#"></a>
        </div>
      </div>

      
   

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
       
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="dashboard.php" id="dashs"><i class="fa fa-dashboard text-green" ></i> <span>Dashboard</span></a></li>
        <li class="active"><a href="#"><i class="fa fa-address-card text-aqua" ></i> <span>Wallet Id</span></a></li>

        
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="javascript:void();" data-toggle="modal" data-target="#modal-recharge"><i class="fa fa-building text-green"  ></i> <span>Recharge Pin</span></a></li>
        <li class="active"><a href="javascript:void();" id="bankdeposit"><i class="fa fa-circle-o text-aqua" ></i> <span>Bank Deposit</span></a></li>
        <li class="active"><a href="javascript:void();" id="wallet_trans"><i class="fa fa-calculator text-aqua" ></i> <span>Transfer to wallet</span></a></li>
        <li class="active"><a href="#"><i class="fa fa-circle-o text-aqua" ></i> <span>Pay Bills</span></a></li>
        <li class="active"><a href="#" id="buyairtimes"><i class="fa fa-wifi text-aqua" ></i> <span>Buy Airtime</span></a></li>
        <li class="active"><a href="#"><i class="fa fa-globe text-orange" ></i> <span>History</span></a></li>
        <li class="treeview">
          <a href="javascript:void();" style="color: #B3C2C4">
            <i class="fa fa-wrench"></i>
            <span >Account Settings</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li class="active"><a href="javascript:void();" data-toggle="modal" data-target="#account-set"><i class="fa fa-circle-o text-aqua" ></i> <span>Change Pin</span></a></li>
            <li><a href="javascript:void();" data-toggle="modal" data-target="#account-phone">  <i class="fa fa-phone"></i> Change Phone number</a></li>
           
          </ul>
        </li>
        <li class="active"><a href="#"><i class="fa fa-circle-o text-aqua" ></i> <span>Customer care</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- MODALS -->
<!-- MODAL FOR RECHARGE Pin-->
<div class="modal fade" id="modal-recharge">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Recharge Pin</h4>
              </div>
              <div class="modal-body">
               <input type="text" name="" id="pin" class="form-control" placeholder="Enter 18 digit pin">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" id="recharge" class="btn btn-primary">Ok</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- MODAL FOR RECHARGE Pin-->
<div class="modal fade" id="account-set">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change Pin</h4>
              </div>
              <div class="modal-body">
              <form id="changepin">
              <div class="form-group has-feedback">
              <input type="password" id="oldpin" name="oldpin" class="form-control" placeholder="old pin">
              <span class=" form-control-feedback"><img id="oldpin-img" style="display: none;" src="img/small.gif"></span>
              </div>
            <input type="password" name="newpin" id="new-pin" class="form-control" placeholder="new pin"><br>

      <div class="form-group has-feedback">
        <input type="password" id="confirm-pin" name="confirmpin" class="form-control" placeholder="confirm pin">
        <span class=" form-control-feedback"><img id="conf-img" style="display: none;" src="img/small.gif"></span>
      </div>
               
               
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" id="ok" name="submit" class="btn btn-primary">Ok</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>

         <!-- MODALS -->
<!-- MODAL FOR CHANGE PHONE-->
<div class="modal fade" id="account-phone">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change Phone number</h4>
              </div>
              <div class="modal-body">
              <p>Your phone number is <span id="c_phone"><?php echo $user['phone']; ?></span></p>
               <input type="text" name="" id="phne" class="form-control" placeholder="Enter new phone number">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" id="chng_phne" class="btn btn-primary">Ok</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
  <!-- END OF MODALS -->