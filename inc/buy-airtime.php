<?php require_once 'sessions.php'; ?>
<?php require_once 'db_connect.php'; ?>
<?php confirm_login(); ?>
<?php require_once 'function.php'; ?>
<?php $user=get_each_user($_SESSION['id']); ?>

<div class="box" style="padding: 10px">
<h4>Buy Airtime</h4>
	<form>
		
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Recipent phone number ">
        <span class="fa fa-user text-gray form-control-feedback"></span>
      </div>
			</div>
			<div class="col-lg-6">
				<div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Amount">
        <span class="fa fa-money text-gray form-control-feedback"></span>
      </div>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-6">
				<div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Remarks">
        <span class="fa fa-pencil text-gray form-control-feedback"></span>
      </div>
			</div>
			<div class="col-lg-6">
				<div class="form-group has-feedback">
        <input type="text" class="form-control" placeholder="Your mDeposit pin">
        <span class="fa fa-ticket text-gray form-control-feedback"></span>
      </div>
			</div>
		</div>
		<div class="row">
				<div class="col-md-6" style="margin-bottom: 1%">
			<button type="button" class="btn btn-block btn-success">Proceed</button>
		</div>
		</div>
		
		
		<p></p>
	</form>

</div>