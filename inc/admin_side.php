<?php require_once 'sessions.php'; ?>
<?php require_once 'db_connect.php'; ?>
<?php require_once 'function.php'; ?>
<?php $user=get_each_user($_SESSION['id']); ?>

<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

      <!-- Sidebar user panel (optional) -->
      <div class="user-panel">
        
        <div class="pull-left" style="color: white">
          <p><?php echo $user['fullname']; ?></p>
          <!-- Status -->
          <a href="#"></a>
        </div>
      </div>

      
   

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
       
        <!-- Optionally, you can add icons to the links -->
        <li class="active"><a href="#" data-toggle="modal" data-target="#modal-add"><i class="fa fa-plus text-green" ></i> <span>Add Card</span></a></li>
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- MODALS -->
<!-- MODAL FOR RECHARGE Pin-->
<div class="modal fade" id="modal-add">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Recharge Pin</h4>
              </div>
              <div class="modal-body">
               <input type="text" name="" required id="amount" class="form-control" placeholder="Enter Amount">
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" id="addpin" class="btn btn-primary">Ok</button>
              </div>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
        <!-- MODAL FOR RECHARGE Pin-->
<div class="modal fade" id="account-set">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Change Pin</h4>
              </div>
              <div class="modal-body">
              <form id="changepin">
              <div class="form-group has-feedback">
              <input type="password" id="oldpin" name="oldpin" class="form-control" placeholder="old pin">
              <span class=" form-control-feedback"><img id="oldpin-img" style="display: none;" src="img/small.gif"></span>
              </div>
            <input type="password" name="newpin" id="new-pin" class="form-control" placeholder="new pin"><br>

      <div class="form-group has-feedback">
        <input type="password" id="confirm-pin" name="confirmpin" class="form-control" placeholder="confirm pin">
        <span class=" form-control-feedback"><img id="conf-img" style="display: none;" src="img/small.gif"></span>
      </div>
               
               
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="submit" id="ok" name="submit" class="btn btn-primary">Ok</button>
              </div>
              </form>
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
  <!-- END OF MODALS -->