<?php require_once 'sessions.php'; ?>
<?php require_once 'db_connect.php'; ?>
<?php confirm_login(); ?>
<?php require_once 'function.php'; ?>
<?php $user=get_each_user($_SESSION['id']); ?>
<?php $balance=get_current_balance($_SESSION['id']); ?>
<p class="pull-right">Your Wallet Id:<b>  <?php echo $user['wallet_id']; ?></b></p>
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3 id="curr"></h3>

              <p>Current Balance</p>
            </div>
            <div class="icon">
              <i class="fa fa-money"></i>
            </div>
            </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>N 0.00</h3>

              <p>Total Deposit</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
             <h3 id="tranx">0</h3>

              <p>Total Transaction</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            
          </div>
        </div>
        <!-- ./col -->
        
        
      </div>