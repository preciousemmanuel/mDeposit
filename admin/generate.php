<?php require_once 'inc/sessions.php'; ?>
<?php confirm_login(); ?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Admin - mDeposit</title>

  <!-- load meta tags and styles -->
  <?php require_once 'inc/header.php'; ?>
</head>

<body class="hold-transition skin-green-light sidebar-mini" style="background-color: #f3f3f4">
<div class="wrapper">
<div id="headers">
  <center><img src="img/6(1).gif"></center>
</div>
  <!-- Left side column. contains the logo and sidebar -->
  <div id="sidebar">
  
</div>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <section class="content container-fluid">
    <div id="main-data">
       <center><img src="img/6(1).gif"></center>
    </div>
   
     
      <!-- transaction summary -->
      <div id="show-datao">
      <center><img src="img/6(1).gif" id="load-img" style="display: none;"></center>
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#activity" data-toggle="tab">Successful Transaction</a></li>
                <li><a href="#timeline" data-toggle="tab">Failed Transaction</a></li>
                <li><a href="#settings" data-toggle="tab">Pending Transaction</a></li>
              </ul>
              <div class="tab-content">
                <div class="active tab-pane" id="activity">
                  <!-- Post -->
                  

                </div>
                <div class="active tab-pane">

                  <!-- /.post -->
                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="timeline">
                  <!-- The timeline -->
                  

                </div>
                <!-- /.tab-pane -->

                <div class="tab-pane" id="settings">
                  
                </div>
                <!-- /.tab-pane -->
              </div>
              <!-- /.tab-content -->
            </div>
            </div>
            <!-- /.nav-tabs-custom -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      mDeposit
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2016 <a href="#">mDeposit</a>.</strong> All rights reserved.
  </footer>

   <!-- Add the sidebar's background. This div must be placed
  immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- REQUIRED JS SCRIPTS -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="js/app.js"></script>
<!-- Optionally, you can add Slimscroll and FastClick plugins.
     Both of these plugins are recommended to enhance the
     user experience. -->
</body>
</html>