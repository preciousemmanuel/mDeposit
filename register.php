
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>mDeposit | Register</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. We have chosen the skin-blue for this starter
        page. However, you can choose any other skin. Make sure you
        apply the skin class to the body tag so the changes take effect. -->
  <link rel="stylesheet" href="dist/css/skins/skin-blue.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet"
        href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
<style type="text/css">
  #overlay{
      width: 100%;
      height: 100%;
      position: absolute;
      top:0;
      left: 0;
      background: rgba(0,0,0,0.9);
      display: none;
      text-align: center;
      transition: all 3s ease-in 10s 
    }
    #load-img-reg{
      position: relative;
  top: 50%;
  transform: translateY(-50%);
    }
    #nd{
      display: none;
    }
    #successMsg{
      color: green;
       position: relative;
  top: 50%;
  transform: translateY(-50%);
    }
</style>
</head>
<body class="hold-transition register-page" style="background: #222D32">
<div class="register-box">
  <div class="register-logo">
    <a href="#"><b style="color: #3B5998">m</b><span style="color: #3C8DBC">Deposit</span></a>
  </div>

  <div class="register-box-body">
    <p class="login-box-msg">Create wallet account</p>
   
      <div class="alert alert-danger" id="nd">
         
            <strong><span id="msg_error"></span></strong>
            </div>
     
    <form id="register">
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="fullname" placeholder="Full name">
        <span class="glyphicon glyphicon-user form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="email" class="form-control" name="email" placeholder="Email(optional)">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="text" class="form-control" name="phone" placeholder="Phone number">
        <span class="glyphicon glyphicon-earphone form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="password" name="password" placeholder="Choose pin">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" id="confirm" name="confirm" placeholder="Retype pin">
        <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
      </div>
      <button type="submit" name="submit" style="background-color: #3B5998" class="btn btn-primary btn-block btn-flat form-control">Create your wallet account</button>
    </form>

    

    <a href="login.php" class="text-center">I already have an account</a>
  </div>
  <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- custom js -->
<script type="text/javascript" src="js/app.js"></script>
<!-- iCheck -->
<script src="plugins/iCheck/icheck.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });
</script>
</body>
</html>
